package swapper;

import java.time.LocalDateTime;

public class Utils {
    private enum LogLevel {
        NONE, SWAPPER_ONLY, EXAMPLES_ONLY, ALL
    }

    private static final LogLevel logLevel = LogLevel.EXAMPLES_ONLY;

    private static void logDebug(String msg) {
        System.out.println("[" + Thread.currentThread().getId() + "] [" + LocalDateTime.now() + "] " + msg);
    }

    public static void logSwapperDebug(String msg) {
        if (logLevel == LogLevel.SWAPPER_ONLY || logLevel == LogLevel.ALL) {
            logDebug(msg);
        }
    }

    public static void logExamplesDebug(String msg) {
        if (logLevel == LogLevel.EXAMPLES_ONLY || logLevel == LogLevel.ALL) {
            logDebug(msg);
        }
    }
}
