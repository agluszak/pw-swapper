package swapper.producersConsumers;

import swapper.Swapper;

import java.util.*;

import static swapper.producersConsumers.State.*;

public class Controller<E> {
    private final Swapper<State> swapper;
    private final int capacity;
    private int filled;
    private final Queue<E> contents;

    public Controller(int capacity) {
        this.capacity = capacity;
        this.filled = 0;
        this.swapper = new Swapper<>();
        this.contents = new LinkedList<>();
        swap(none(), list(MUTEX, NOT_FULL));
    }

    private void swap(Collection<State> removed, Collection<State> added) {
        try {
            swapper.swap(removed, added);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static Collection<State> none() {
        return Collections.emptyList();
    }

    private static Collection<State> list(State... states) {
        return Arrays.asList(states);
    }

    public void add(E item) {
        swap(list(NOT_FULL, MUTEX), none());
        contents.add(item);
        filled++;
        if (filled == capacity) {
            System.out.println("Bufor pełny!");
            swap(none(), list(NOT_EMPTY, MUTEX));
        } else {
            swap(none(), list(NOT_EMPTY, NOT_FULL, MUTEX));
        }
    }

    public E poll() {
        swap(list(NOT_EMPTY, MUTEX), none());
        assert !contents.isEmpty();
        E item = contents.remove();
        filled--;
        if (filled == 0) {
            System.out.println("Bufor pusty!");
            swap(none(), list(NOT_FULL, MUTEX));
        } else {
            swap(none(), list(NOT_EMPTY, NOT_FULL, MUTEX));
        }
        return item;
    }

}
