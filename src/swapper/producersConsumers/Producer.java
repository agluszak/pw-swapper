package swapper.producersConsumers;

import java.util.function.Supplier;

public class Producer<E> implements Runnable {
    private final long wait;
    private final Controller<E> controller;
    private final Supplier<E> supplier;

    public Producer(long wait, Controller<E> controller, Supplier<E> supplier) {
        this.wait = wait;
        this.controller = controller;
        this.supplier = supplier;
    }

    @Override
    public void run() {
        try {
            while (true) {
                Thread.sleep(wait);
                controller.add(supplier.get());
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
