package swapper.producersConsumers;

import static swapper.Utils.logExamplesDebug;

public class ProducersConsumers {
    public static void main(String[] args) {
        Controller<Integer> controller = new Controller<>(5);

        Thread onesProducer = new Thread(new Producer<>(100, controller, () -> 1));
        Thread twosProducer = new Thread(new Producer<>(200, controller, () -> 2));
        Thread piProducer = new Thread(new Producer<>(1000, controller, () -> 314));

        Thread standardConsumer = new Thread(new Consumer<>(150, controller, (num) -> logExamplesDebug("hello " + num)));
        Thread angryConsumer = new Thread(new Consumer<>(500, controller, (num) -> logExamplesDebug("what? a " + num + "?!")));


        onesProducer.start();
        twosProducer.start();
        piProducer.start();
        standardConsumer.start();
        angryConsumer.start();
    }


}
