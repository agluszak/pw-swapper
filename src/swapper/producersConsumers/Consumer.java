package swapper.producersConsumers;


public class Consumer<E> implements Runnable {
    private final long wait;
    private final Controller<E> controller;
    private final java.util.function.Consumer<E> consumer;

    public Consumer(long wait, Controller<E> controller, java.util.function.Consumer<E> consumer) {
        this.wait = wait;
        this.controller = controller;
        this.consumer = consumer;
    }

    @Override
    public void run() {
        try {
            while (true) {
                Thread.sleep(wait);
                E item = controller.poll();
                consumer.accept(item);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
