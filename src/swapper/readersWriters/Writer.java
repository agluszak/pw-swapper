package swapper.readersWriters;

import java.util.List;
import java.util.function.UnaryOperator;

public class Writer<E> implements Runnable {
    private final long wait;
    private final long workTime;
    private final Controller<E> controller;
    private final UnaryOperator<List<E>> transform;
    private final Runnable before;
    private final Runnable after;

    public Writer(long wait, long workTime, Controller<E> controller, UnaryOperator<List<E>> transform, Runnable before, Runnable after) {
        this.wait = wait;
        this.workTime = workTime;
        this.controller = controller;
        this.transform = transform;
        this.before = before;
        this.after = after;
    }

    @Override
    public void run() {
        try {
            while (true) {
                Thread.sleep(wait);
                controller.startWriting();
                before.run();
                Thread.sleep(workTime);
                controller.write(transform);
                after.run();
                controller.finishWriting();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
