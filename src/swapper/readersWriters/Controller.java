package swapper.readersWriters;

import swapper.Swapper;

import java.util.*;
import java.util.function.UnaryOperator;

import static swapper.readersWriters.State.*;

public class Controller<E> {
    private final Swapper<State> swapper;
    private final List<E> contents;
    private int readersWaiting;
    private int writersWaiting;
    private int readersReading;
    private boolean writerWriting;

    public Controller() {
        this.swapper = new Swapper<>();
        this.contents = new ArrayList<>();
        this.readersReading = 0;
        this.readersWaiting = 0;
        this.writersWaiting = 0;
        this.writerWriting = false;
        swap(none(), list(MUTEX, CAN_READ, CAN_WRITE));
    }

    private void swap(Collection<State> removed, Collection<State> added) {
        try {
            swapper.swap(removed, added);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static Collection<State> none() {
        return Collections.emptyList();
    }

    private static Collection<State> list(State... states) {
        return Arrays.asList(states);
    }

    public void startReading() {
        swap(list(MUTEX), none());
        if (writerWriting || writersWaiting > 0) {
            readersWaiting++;
            swap(none(), list(MUTEX));
            swap(list(CAN_READ, MUTEX), none());
            readersWaiting--;
        }
        readersReading++;
        swap(none(), list(MUTEX, CAN_READ));

    }

    public void finishReading() {
        swap(list(MUTEX), none());
        readersReading--;
        if (readersReading == 0) {
            swap(none(), list(CAN_WRITE));
        }
        swap(none(), list(MUTEX));

    }

    public List<E> read() {
        return new ArrayList<>(contents);
    }

    public void startWriting() {
        swap(list(MUTEX), none());
        if (writerWriting || readersReading > 0) {
            writersWaiting++;
            swap(none(), list(MUTEX));
            swap(list(CAN_WRITE, MUTEX), none());
            writersWaiting--;
        }
        writerWriting = true;
    }

    public void finishWriting() {
        writerWriting = false;
        if (readersWaiting > 0) {
            swap(none(), list(CAN_READ, MUTEX));
        } else {
            swap(none(), list(CAN_WRITE, MUTEX));
        }
    }

    public void write(UnaryOperator<List<E>> transform) {
        List<E> result = transform.apply(contents);
        contents.clear();
        contents.addAll(result);
    }

}
