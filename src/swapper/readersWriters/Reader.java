package swapper.readersWriters;

import java.util.List;

public class Reader<E> implements Runnable {
    private final long wait;
    private final long workTime;
    private final Controller<E> controller;
    private final java.util.function.Consumer<List<E>> consumer;

    public Reader(long wait, long workTime, Controller<E> controller, java.util.function.Consumer<List<E>> consumer) {
        this.wait = wait;
        this.workTime = workTime;
        this.controller = controller;
        this.consumer = consumer;
    }

    @Override
    public void run() {
        try {
            while (true) {
                Thread.sleep(wait);
                controller.startReading();
                Thread.sleep(workTime);
                List<E> items = controller.read();
                consumer.accept(items);
                controller.finishReading();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
