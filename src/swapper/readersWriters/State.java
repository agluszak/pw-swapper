package swapper.readersWriters;

public enum State {
    CAN_READ, CAN_WRITE, MUTEX
}
