package swapper.readersWriters;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import static swapper.Utils.logExamplesDebug;

public class ReadersWriters {
    public static void main(String[] args) {
        Controller<Integer> controller = new Controller<>();

        Thread allItemsReader = new Thread(new Reader<>(100, 15, controller,
                (items) -> logExamplesDebug(items.toString())
        ));

        Thread onlySecondItemReader = new Thread(new Reader<>(150, 30, controller,
                (items) -> {
                    if (items.size() < 2) {
                        logExamplesDebug("There is no second element :(");
                    } else {
                        logExamplesDebug("Second element is " + items.get(1));
                    }
                }
        ));

        Thread sumReader = new Thread(new Reader<>(300, 100, controller,
                (items) -> {
                    int sum = 0;
                    for (int i : items) {
                        sum += i;
                    }
                    logExamplesDebug("Sum: " + sum);
                }
        ));


        Thread bigNumAdder = new Thread(new Writer<>(1000, 300, controller,
                (items) -> {
                    List<Integer> withAddedNum = new ArrayList<>(items);
                    withAddedNum.add(1000);
                    return withAddedNum;
                },
                () -> logExamplesDebug("Will add 1000"),
                () -> logExamplesDebug("Added 1000")
        ));

        Thread oneAdder = new Thread(new Writer<>(200, 10, controller,
                (items) -> {
                    List<Integer> withAddedNum = new ArrayList<>(items);
                    withAddedNum.add(1);
                    return withAddedNum;
                },
                () -> logExamplesDebug("Will add 1"),
                () -> logExamplesDebug("Added 1")
        ));

        Thread cleaner = new Thread(new Writer<>(10000, 2000, controller,
                (items) -> new ArrayList<>(),
                () -> logExamplesDebug("Cleaning time!"),
                () -> logExamplesDebug("Cleaned.")
        ));

        Thread randomAdder = new Thread(new Writer<>(350, 100, controller,
                (items) -> {
                    List<Integer> withAddedNum = new ArrayList<>(items);
                    withAddedNum.add(ThreadLocalRandom.current().nextInt(1000));
                    return withAddedNum;
                },
                () -> logExamplesDebug("Will add some random number"),
                () -> logExamplesDebug("Added some random number")
        ));

        allItemsReader.start();
        onlySecondItemReader.start();
        sumReader.start();
        bigNumAdder.start();
        oneAdder.start();
        cleaner.start();
        randomAdder.start();

    }
}
