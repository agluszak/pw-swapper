package swapper;

import java.util.*;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static swapper.Utils.logSwapperDebug;


/**
 * Ogólna idea:
 * Swapper posiada mapę ze zbiorów które mają być usunięte na obiekt typu Waiter, na którym wątki oczekują.
 * Po obudzeniu i upewnieniu się, że w Swapperze są już wymagane elementy, wątek usuwa elementy kolekcji removed,
 * dodaje elementy kolekcji added, a na koniec przechodzi po całej mapie i sprawdza,
 * czy można kogoś obudzić - jeśli tak, to budzi ten jeden konkretny wątek czekający na Waiterze.
 */
public class Swapper<E> {

    /**
     * Obiekt, na którym "wieszają" się wątki oczekujące na wypełnienie swappera elementami.
     * Zlicza ile wątków oczekuje na dany zbiór
     */
    private class Waiter {
        private int counter;
        private final Condition condition;

        Waiter() {
            this.condition = lock.newCondition();
            counter = 1;
        }

        void increaseCounter() {
            counter++;
        }

        void decreaseCounter() {
            counter--;
        }

        int getCounter() {
            return counter;
        }

        void await() throws InterruptedException {
            condition.await();
        }

        void signal() {
            condition.signal();
        }
    }

    private final Set<E> contents;
    private final Lock lock;
    private final HashMap<HashSet<E>, Waiter> waiters;

    public Swapper() {
        contents = new HashSet<>();
        lock = new ReentrantLock(true);
        waiters = new HashMap<>();
    }

    private void addWaiter(HashSet<E> removedSet) {
        if (waiters.containsKey(removedSet)) {
            waiters.get(removedSet).increaseCounter();
        } else {
            waiters.put(removedSet, new Waiter());
        }
    }

    public void swap(Collection<E> removed, Collection<E> added) throws InterruptedException {
        HashSet<E> removedSet = new HashSet<>(removed);
        HashSet<E> addedSet = new HashSet<>(added);

        lock.lock();
        logSwapperDebug("Przychodzi wątek co chce usunąć " + removedSet.toString() + " i dodać " + addedSet.toString());
        logSwapperDebug("Stan swappera:" + contents.toString());
        addWaiter(removedSet);
        Waiter waiter = waiters.get(removedSet);

        try {
            while (!contents.containsAll(removed)) {
                logSwapperDebug("Idzie spać wątek co chce usunąć " + removedSet.toString() + " i dodać " + addedSet.toString());
                waiter.await();
            }

            if (!Thread.currentThread().isInterrupted()) {
                logSwapperDebug("Może zrobić co ma zrobić wątek co chce usunąć " + removedSet.toString() + " i dodać " + addedSet.toString());
                contents.removeAll(removedSet);
                contents.addAll(added);
                logSwapperDebug("Stan swappera:" + contents.toString());
            } else {
                throw new InterruptedException();
            }
        } catch (InterruptedException e) {
            logSwapperDebug("Niestety przerwał się wątek co chce usunąć " + removedSet.toString() + " i dodać " + addedSet.toString());
            throw e;
        } finally {
            waiter.decreaseCounter();
            if (waiter.getCounter() == 0) {
                waiters.remove(removedSet);
            }

            for (Map.Entry<HashSet<E>, Waiter> entry : waiters.entrySet()) {
                if (contents.containsAll(entry.getKey())) {
                    logSwapperDebug("Budzi kogoś innego wątek co chce usunąć " + removedSet.toString() + " i dodać " + addedSet.toString());
                    entry.getValue().signal();
                    break;
                }
            }
            lock.unlock();
        }

    }


}