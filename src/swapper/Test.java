package swapper;

import java.util.Arrays;
import java.util.Collections;

public class Test {
    private final static Swapper<Integer> swapper = new Swapper<>();


    public static void main(String[] args) {

        Thread t1 = new Thread(() -> {
            try {
                swapper.swap(Collections.emptyList(), Arrays.asList(1, 2));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        Thread t2 = new Thread(() -> {
            try {
                swapper.swap(Arrays.asList(1, 2, 3), Arrays.asList(6));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        Thread t3 = new Thread(() -> {
            try {
                swapper.swap(Arrays.asList(1), Arrays.asList(1, 3));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        Thread t4 = new Thread(() -> {
            try {
//                Thread.sleep(1000);
                swapper.swap(Arrays.asList(1, 2, 3), Arrays.asList(5));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        Thread t5 = new Thread(() -> {
            try {
                swapper.swap(Arrays.asList(5), Arrays.asList(1, 2, 3, 666));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        t1.start();

//        t2.start();
        t3.start();
        t4.start();

        t5.start();

        try {
            Thread.sleep(10);
            t4.interrupt();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
