# PW-swapper
Swapper to zbiór, na którym można wykonać niepodzielną operację usunięcia a następnie dodania obiektów.

W pakiecie swapper, implementującym w Javie swapper wartości typu E, jest definicja klasy Swapper<E>:

Bezparametrowy konstruktor tworzy swapper, który w stanie początkowym jest pusty.

Metoda swap(removed, added) wstrzymuje wątek do chwili, gdy w swapperze będą wszystkie elementy kolekcji removed. Następnie, niepodzielnie:

usuwa ze swappera wszystkie elementy kolekcji removed, po czym

dodaje do swappera wszystkie elementy kolekcji added.

Kolekcje removed i added mogą mieć niepuste przecięcie.

Elementy swappera nie powtarzają się. Dodanie do swappera obiektu, który już w nim jest, nie ma żadnego efektu.

Zarówno kolekcja removed jak i added może mieć powtórzenia. Nie wpływają one na działanie metody.

W przypadku przerwania wątku metoda zgłasza wyjątek InterruptedException.

Przerwane wykonanie metody nie zmienia zawartości swappera.

Przerwanie wątku korzystającego ze swappera nie wpływa na poprawność działania pozostałych wątków.


